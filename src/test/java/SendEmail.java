import org.testng.annotations.Test;
import utilities.ReadFromFile;

import static org.testng.Assert.assertTrue;

public class SendEmail extends BaseTest {

    private final String IUA_EMAIL = "q160822@i.ua";

    private final String UKRNET_CORRECT_PAGE = "https://mail.ukr.net/";

    private final String UKRNET_URL = "https://accounts.ukr.net/login";

    private final String TOPIC = "Email from Q";

    private final String TEXT = "Hello! I wish you all the best. Best regards, Q.";

    @Test
    public void checkSendMail() {
        ReadFromFile data = new ReadFromFile();
        data.readFromFile();
        //1) Log in to a ukr.net website
        getUkrNetPage().openUkrNetPage(UKRNET_URL);
        getUkrNetPage().isInputEmailFieldVisible();
        getUkrNetPage().isInputPasswordFieldVisible();
        getUkrNetPage().isNextButtonVisible();
        getUkrNetPage().inputEmail(data.getLogin());
        getUkrNetPage().inputPassword(data.getPassword());
        getUkrNetPage().clickOnNextButton();
        getUkrNetPage().implicitWait(1000);
        getUkrNetPage().isWriteLetterButtonVisible();
        assertTrue(getDriver().getCurrentUrl().contains(UKRNET_CORRECT_PAGE));
        //2) Create a simple mail
        getUkrNetPage().isWriteLetterButtonVisible();
        getUkrNetPage().clickOnWriteLetterButton();
        getUkrNetPage().isInputNewMailEmailFieldVisible();
        getUkrNetPage().inputNewMailEmailFieldVisible(IUA_EMAIL);
        getUkrNetPage().isInputNewMailTopicFieldVisible();
        getUkrNetPage().inputNewMailTopicFieldVisible(TOPIC);
        getUkrNetPage().isInputNewMailTextFieldVisible();
        getUkrNetPage().inputNewMailTextFieldVisible(TEXT);
        //3) Send this mail to another test user
        getUkrNetPage().isSendEmailButtonVisible();
        getUkrNetPage().clickOnSendEmailButton();
        getUkrNetPage().waitVisibilityOfElement(300, getUkrNetPage().getSendEmailMessage());
        assertTrue(getUkrNetPage().isSendEmailMessageVisible());
    }
}
