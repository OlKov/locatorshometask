package utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadFromFile {

    String login;
    String password;

    public ReadFromFile() {
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void readFromFile(){
        File loginData = new File("src/main/java/utilities/LoginData.txt");
        try{
            Scanner read = new Scanner(loginData);
            read.useDelimiter(",");
            while(read.hasNext()){
                login = read.next();
                password = read.next();
            }
            read.close();
        }
        catch (FileNotFoundException qwerty){
            System.out.println("File not found");
        }
    }
}
