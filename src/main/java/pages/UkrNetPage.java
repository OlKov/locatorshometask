package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.openqa.selenium.By.*;

public class UkrNetPage extends BasePage {

    private static final String EMAIL_FIELD = "login";

    private static final String PASSWORD_FIELD = "password";

    private static final String NEXT_BUTTON = "//button[@type='submit']";

    private static final String WRITE_LETTER_BUTTON ="//button[@class='button primary compose']";

    private static final String NEW_MAIL_EMAIL_FIELD = "toFieldInput";

    private static final String NEW_MAIL_TOPIC_FIELD = "subject";
    private static final String NEW_MAIL_TEXT_FIELD = "mce_0_ifr";

    private static final String SEND_EMAIL_BUTTON = "button[class='button primary send']";

    private static final String SEND_EMAIL_MESSAGE = "div[class='sendmsg__ads-ready']";

    public boolean isSendEmailMessageVisible() {
        return  driver.findElement(By.cssSelector(SEND_EMAIL_MESSAGE)).isDisplayed();
    }

    public By getSendEmailMessage(){
        return By.cssSelector(SEND_EMAIL_MESSAGE);
    }

    public void isSendEmailButtonVisible() {
        driver.findElement(By.cssSelector(SEND_EMAIL_BUTTON)).isDisplayed();
    }

    public void clickOnSendEmailButton() {
        driver.findElement(By.cssSelector(SEND_EMAIL_BUTTON)).click();
    }

    public void isInputNewMailEmailFieldVisible() {
        driver.findElement(By.name(NEW_MAIL_EMAIL_FIELD)).isDisplayed();
    }

    public void isInputNewMailTopicFieldVisible() {
        driver.findElement(By.name(NEW_MAIL_TOPIC_FIELD)).isDisplayed();
    }

    public void isInputNewMailTextFieldVisible() {
        driver.findElement(By.id(NEW_MAIL_TEXT_FIELD)).isDisplayed();
    }

    public void inputNewMailEmailFieldVisible(final String keyword) {
        driver.findElement(By.name(NEW_MAIL_EMAIL_FIELD)).sendKeys(keyword);
    }

    public void inputNewMailTopicFieldVisible(final String keyword) {
        driver.findElement(By.name(NEW_MAIL_TOPIC_FIELD)).sendKeys(keyword);
    }

    public void inputNewMailTextFieldVisible(final String keyword) {

        driver.findElement(By.id(NEW_MAIL_TEXT_FIELD)).sendKeys(keyword);
    }

    public UkrNetPage(WebDriver driver) {
        super(driver);
    }

    public void openUkrNetPage(String url) {
        driver.get(url);
    }

    public void isInputEmailFieldVisible() {
        driver.findElement(name(EMAIL_FIELD)).isDisplayed();
    }

    public void isNextButtonVisible() {
        driver.findElement(xpath(NEXT_BUTTON)).isDisplayed();
    }

    public void clickOnNextButton() {
        driver.findElement(xpath(NEXT_BUTTON)).click();
    }
    public void inputEmail(final String keyword) {
        driver.findElement(name(EMAIL_FIELD)).sendKeys(keyword);
    }

    public void isInputPasswordFieldVisible() {
        driver.findElement(name(PASSWORD_FIELD)).isDisplayed();
    }

    public void inputPassword(final String keyword) {
        driver.findElement(name(PASSWORD_FIELD)).sendKeys(keyword);
    }

    public void isWriteLetterButtonVisible(){
        driver.findElement(xpath(WRITE_LETTER_BUTTON)).isDisplayed();
    }

    public void clickOnWriteLetterButton(){
        driver.findElement(xpath(WRITE_LETTER_BUTTON)).click();
    }


}